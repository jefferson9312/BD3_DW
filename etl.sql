DROP EVENT IF EXISTS DW_FILL_MES;
DROP PROCEDURE IF EXISTS DW_FATOVENDAS_MES;
DROP PROCEDURE IF EXISTS DW_FATOCOMPRAS_MES;
DROP PROCEDURE IF EXISTS DW_FATOGASTOCLIENTE_MES;
DROP PROCEDURE IF EXISTS DW_FATOLUCRO_MES;
DROP PROCEDURE IF EXISTS DW_FATOATENDIMENTO_MES;
DROP PROCEDURE IF EXISTS DW_TODOSFATOS_ANUAL;

DROP TRIGGER IF EXISTS TG_DW_CLIENTE;
DROP TRIGGER IF EXISTS TG_DW_ULTIMA_VENDA;
DROP TRIGGER IF EXISTS TG_DW_FUNCIONARIO;
DROP TRIGGER IF EXISTS TG_DW_LOJA;


#criar triggers tabelas sem fk

# A cada vez q um cliente for inserido vai ser atualizado no DW
delimiter //
CREATE TRIGGER TG_DW_CLIENTE after insert
ON tb010_clientes
for each row
begin
	insert into cliente value (new.tb010_cpf, new.tb010_nome, null);
end//
delimiter ;

# A cada vez q uma loja for inserida vai ser atualizado no DW
delimiter //
CREATE TRIGGER TG_DW_LOJA after insert
ON tb004_lojas
for each row
begin
	insert into loja value (new.tb004_cod_loja, new.tb004_cnpj_loja);
end//
delimiter ;

# A cada vez q uma venda for efetuada vai ser atualizado no DW
delimiter //
CREATE TRIGGER TG_DW_ULTIMA_VENDA after insert
ON tb010_012_vendas
for each row
begin
	update cliente set ultima_venda = new.tb010_012_data where cpf = new.tb010_cpf;
end//
delimiter ;

# A cada vez q um funcionario for inserido vai ser atualizado no DW
delimiter //
CREATE TRIGGER TG_DW_FUNCIONARIO after insert
ON tb005_funcionarios
for each row
begin
	insert into funcionario value (new.tb005_matricula, new.tb005_nome_completo);
end//
delimiter ;
 
# Evento executado mensalmente no primeiro dia de cada mes para atualizar os dados mensais do DW
DELIMITER //
CREATE EVENT DW_FILL_MES
ON SCHEDULE EVERY '1' MONTH
STARTS '2023-01-01 00:00:00'
DO 
BEGIN
	#se for o 1° mes do ano então o DW atualiza as tabelas fatos que possuem a granularidade por Ano
	IF month(NOW()) = 1 THEN
		CALL DW_TODOSFATOS_ANUAL();
    END IF;
    CALL DW_FATOVENDAS_MES();
    CALL DW_FATOCOMPRAS_MES();
    CALL DW_FATOGASTOCLIENTE_MES();
    CALL DW_FATOLUCRO_MES();
    CALL DW_FATOATENDIMENTO_MES();
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_FATOVENDAS_MES()
BEGIN
	#POR DIA DA SEMANA, DIA, MES E ANO
	insert into fatovendas
	select year(v.tb010_012_data), month(v.tb010_012_data), day(v.tb010_012_data), weekday(v.tb010_012_data),
	v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja
	from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
	where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja AND v.tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
    group by v.tb012_cod_produto, v.tb010_012_data, l.tb004_cod_loja;

	#Granularidade por dia da Semana DE CADA MES
	insert into fatovendas
	select year(v.tb010_012_data), month(v.tb010_012_data), null, weekday(v.tb010_012_data),
	v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja
	from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
	where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja AND v.tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
	group by v.tb012_cod_produto, weekday(v.tb010_012_data), month(v.tb010_012_data), year(v.tb010_012_data), l.tb004_cod_loja;

	#Granularidade por mes
	insert into fatovendas
	select year(v.tb010_012_data), month(v.tb010_012_data), null, null,
	v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja
	from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
	where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja AND v.tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
	group by v.tb012_cod_produto, month(v.tb010_012_data), year(v.tb010_012_data), l.tb004_cod_loja;
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_FATOCOMPRAS_MES()
BEGIN
	#Fato Compra de Produtos
	#Ganularidade Total - COMPRAS POR MES E ANO
	insert into fatocompras
	select year(c.tb012_017_data), month(c.tb012_017_data), c.tb012_cod_produto, 
	sum(c.tb012_017_quantidade), sum(c.tb012_017_valor_unitario)
	from tb012_017_compras c where c.tb012_017_data between DATE_SUB(NOW(), interval 1 month) and NOW()
	group by c.tb012_cod_produto;
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_FATOGASTOCLIENTE_MES()
BEGIN
	#Fato Gasto Cliente
	#Não é possível ver qual a forma de pagamento porque n existe campo que indique isso
	#Será feito gasto do cliente por periodo de tempo
	#Granularidade Total pro mes e ano
	insert into fatogastocliente
	select year(v.tb010_012_data), month(v.tb010_012_data), v.tb010_cpf, sum(v.tb010_012_quantidade) qtde, 
	sum(v.tb010_012_valor_unitario) valor, (v.tb010_012_valor_unitario/dayofmonth(LAST_DAY(v.tb010_012_data))) as media_por_dia
	from tb010_012_vendas v where v.tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
	group by v.tb010_cpf, month(v.tb010_012_data) order by v.tb010_012_valor_unitario desc limit 20;
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_FATOLUCRO_MES()
BEGIN
	#Lucro por produto por mes de cada ano
	insert into fatolucratividade
	select AnoNum, mesAno, cod_prod ,(receita - custo) 
    from tempFatoLucro where data2 between DATE_SUB(NOW(), interval 1 month) and NOW();
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_FATOATENDIMENTO_MES()
BEGIN
	#Atendimentos por dia, mes e ano
	insert into fatoatendimento
	select year(tb010_012_data) ano, month(tb010_012_data) mes, day(tb010_012_data) dia,
	tb005_matricula matricula, count(tb010_012_data) qtde 
	from tb010_012_vendas where tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
	 group by tb005_matricula, year(tb010_012_data), month(tb010_012_data), day(tb010_012_data);
     
     #Atendimentos por mês
	insert into fatoatendimento
	select year(tb010_012_data) ano, month(tb010_012_data) mes, null dia,
	tb005_matricula matricula, count(tb010_012_data) qtde 
	from tb010_012_vendas where tb010_012_data between DATE_SUB(NOW(), interval 1 month) and NOW()
    group by tb005_matricula, year(tb010_012_data), month(tb010_012_data);
END//
DELIMITER ;

DELIMITER //
CREATE PROCEDURE DW_TODOSFATOS_ANUAL()
BEGIN
	#Granularidade por ano - Fato Vendas
	insert into fatovendas
	select year(v.tb010_012_data), null, null, null,
	v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja
	from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
	where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja AND v.tb010_012_data between DATE_SUB(NOW(), interval 1 year) and NOW()
	group by v.tb012_cod_produto, year(v.tb010_012_data), l.tb004_cod_loja;

	#Ganularidade Ano - Fato Compra
	insert into fatocompras
	select year(c.tb012_017_data), null, c.tb012_cod_produto, 
	sum(c.tb012_017_quantidade), sum(c.tb012_017_valor_unitario)
	from tb012_017_compras c where c.tb012_017_data between DATE_SUB(NOW(), interval 1 year) and NOW()
	group by c.tb012_cod_produto, year(c.tb012_017_data);
    
    #Granularidade por ano - Gasto Cliente
	insert into fatogastocliente
	select year(v.tb010_012_data), null, v.tb010_cpf, sum(v.tb010_012_quantidade) qtde, 
	sum(v.tb010_012_valor_unitario) valor, (v.tb010_012_valor_unitario/12) as media_por_mes
	from tb010_012_vendas v where v.tb010_012_data between DATE_SUB(NOW(), interval 1 year) and NOW()
	group by v.tb010_cpf, year(v.tb010_012_data) order by v.tb010_012_valor_unitario desc limit 20;
    
	#Lucro por produto por ano - fatolucratividade
	insert into fatolucratividade
	select AnoNum, null, cod_prod , (sum(receita)-sum(custo)) 'Lucro Anual' 
	from tempFatoLucro where data2 between DATE_SUB(NOW(), interval 1 year) and NOW()
    group by cod_prod, AnoNum;
    
    #Fato Atendimento por ano
    insert into fatoatendimento
	select year(tb010_012_data) ano, null mes, null dia,
	tb005_matricula matricula, count(tb010_012_data) qtde
	from tb010_012_vendas where tb010_012_data between DATE_SUB(NOW(), interval 1 year) and NOW()
    group by tb005_matricula, year(tb010_012_data);
END//
DELIMITER ;
