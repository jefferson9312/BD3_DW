create table mes(
mes integer primary key,
nome varchar(50) not null
);

create table diaSemana(
diaSemana integer primary key,
nome varchar(20) not null
);

create table produto(
cod_prod INT(10) primary key,
descricao varchar(255) not null
);

create table categoria(
cod_categoria INT(10) primary key,
descricao varchar(255) not null
);

CREATE TABLE cliente( 
cpf NUMERIC(15) primary key,
nome VARCHAR(255) NOT NULL
);

alter table cliente add column ultima_venda datetime;


CREATE TABLE funcionario( 
matricula INT(10) primary key,
nome VARCHAR(255) NOT NULL
);

CREATE TABLE loja(
cod INT(10) primary key,
cnpj VARCHAR(20)  NOT NULL
);


create table fatoCompras(
ano integer,
mes integer,
cod_prod INT(10),
qtde integer,
valor_total numeric(10,2) NULL,
constraint fk_compra2 foreign key(mes) references mes(mes),
constraint fk_compra3 foreign key(cod_prod) references produto(cod_prod)
);

create table fatoLucratividade(
ano integer,
mes integer,
cod_prod INT(10),
lucro numeric(10,2),
constraint fk_lucro2 foreign key(mes) references mes(mes),
constraint fk_lucro3 foreign key(cod_prod) references produto(cod_prod)
);


create table fatoVendasAtendGasto(
ano integer,
mes integer,
dia integer,
diaSemana integer,
cod_prod INT(10),
cod_categoria INT(10),
qtde integer,
cod_loja INT(10),
cpf NUMERIC(15),
matricula INT(10),
valor numeric(10,2),
media_gasto numeric(10,2),
constraint fk_vf2 foreign key(mes) references mes(mes),
constraint fk_vf3 foreign key(diaSemana) references diaSemana(diaSemana),
constraint fk_vf4 foreign key(cod_prod) references produto(cod_prod),
constraint fk_vf5 foreign key(cod_categoria) references categoria(cod_categoria),
constraint fk_vf6 foreign key(cod_loja) references loja(cod),
constraint fk_vf7 foreign key(cpf) references cliente(cpf),
constraint fk_vf8 foreign key(matricula) references funcionario(matricula)
);


#Views criadas para facilitar as operações no DW

#facilita insert wm fatolucratividade
create view tempFatoLucro as
	select ccod2 cod_prod, cmes2 mesAno, cano2 AnoNum, data2, ifnull(ccomp, 0) custo, ifnull(vvend, 0) receita from
	(select comp.ccod ccod2, comp.cmes cmes2, comp.cano cano2, comp.data2 ,sum(comp.compra) ccomp from(
	select c.tb012_cod_produto ccod, c.tb012_017_data data2, month(c.tb012_017_data) cmes, year(c.tb012_017_data) cano, (sum(c.tb012_017_quantidade)*sum(c.tb012_017_valor_unitario)) compra
	from tb012_017_compras c group by c.tb012_cod_produto, month(c.tb012_017_data), year(c.tb012_017_data), c.tb012_017_valor_unitario
	) comp group by comp.ccod, comp.cmes, comp.cano) cp left join
	(select vend.vcod vcod2, vend.vmes vmes2, vend.vano vano2, sum(vend.venda) vvend from(
	select v.tb012_cod_produto vcod, month(v.tb010_012_data) vmes,  year(v.tb010_012_data) vano, (sum(v.tb010_012_valor_unitario)*sum(v.tb010_012_quantidade)) venda
	from tb010_012_vendas v group by v.tb012_cod_produto, month(v.tb010_012_data), year(v.tb010_012_data), v.tb010_012_valor_unitario
	) vend group by vend.vcod, vend.vmes, vend.vano) vd
	on vd.vcod2 = cp.ccod2 and vd.vano2 = cp.cano2 and vd.vmes2 = cp.cmes2;

	#Fato Ultima compra do cliente
	create view fatoUltimaCompra as 
	select cpf, nome, datediff(curdate(), ultima_venda) 'Dias da Ultima Venda' from cliente;

#Inserts Iniciais que populam as tabelas sem Fks no DW
insert into produto select tb012_cod_produto, tb012_descricao from tb012_produtos;
insert into categoria select tb013_cod_categoria, tb013_descricao from tb013_categorias;
insert into cliente select tb010_cpf, tb010_nome, null from tb010_clientes;
insert into diaSemana values (0, 'Segunda'), (1, 'Terça'), (2, 'Quarta'), (3, 'Quinta'), (4, 'Sexta'), (5, 'Sábado'), (6, 'Domingo');
insert into mes values (1 , 'JANEIRO'),(2 , 'FEVEREIRO'),(3 , 'MARÇO'),(4 , 'ABRIL'),(5 , 'MAIO'),(6 , 'JUNHO'),(7 , 'JULHO'),(8 , 'AGOSTO'),(9 , 'SETEMBRO'),(10, 'OUTUBRO'),(11, 'NOVEMBRO'),(12, 'DEZEMBRO');
insert into funcionario select tb005_matricula, tb005_nome_completo from tb005_funcionarios;
update cliente set ultima_venda = (select max(v.tb010_012_data) from tb010_012_vendas v where v.tb010_cpf = cliente.cpf group by v.tb010_cpf);
insert into loja select tb004_cod_loja, tb004_cnpj_loja from tb004_lojas;


#Inserts que populam as tabelas com fks no DW


#Fato Compra de Produtos
#Ganularidade Total
insert into fatocompras
select year(c.tb012_017_data), month(c.tb012_017_data), c.tb012_cod_produto, 
sum(c.tb012_017_quantidade), sum(c.tb012_017_valor_unitario)
from tb012_017_compras c
group by c.tb012_cod_produto;

#Ganularidade Ano
insert into fatocompras
select year(c.tb012_017_data), null, c.tb012_cod_produto, 
sum(c.tb012_017_quantidade), sum(c.tb012_017_valor_unitario)
from tb012_017_compras c
group by c.tb012_cod_produto, year(c.tb012_017_data);



#Fato Vendas
#Granularidade Total
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), month(v.tb010_012_data), day(v.tb010_012_data), weekday(v.tb010_012_data),
v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja, null,null,null,null
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb012_cod_produto, v.tb010_012_data, l.tb004_cod_loja;

#Granularidade por dia da Semana de cada mes de cada ano
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), month(v.tb010_012_data), null, weekday(v.tb010_012_data),
v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja, null,null,null,null
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb012_cod_produto, weekday(v.tb010_012_data), month(v.tb010_012_data), year(v.tb010_012_data), l.tb004_cod_loja;

#Granularidade por mes
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), month(v.tb010_012_data), null, null,
v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja, null,null,null,null
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb012_cod_produto, month(v.tb010_012_data), year(v.tb010_012_data), l.tb004_cod_loja;

#Granularidade por ano
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), null, null, null,
v.tb012_cod_produto, p.tb013_cod_categoria, sum(v.tb010_012_quantidade), l.tb004_cod_loja, null,null,null,null
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb012_cod_produto, year(v.tb010_012_data), l.tb004_cod_loja;


#Fato Gasto Cliente
#Não é possível ver qual a forma de pagamento porque n existe campo que indique isso
#Será feito gasto do cliente por periodo de tempo e loja

#Granularidade Total
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), month(v.tb010_012_data), null, null, null, null, sum(v.tb010_012_quantidade) qtde, l.tb004_cod_loja, v.tb010_cpf, null,
sum(v.tb010_012_valor_unitario) valor, (v.tb010_012_valor_unitario/dayofmonth(LAST_DAY(v.tb010_012_data))) as media_por_dia
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb010_cpf, month(v.tb010_012_data) order by v.tb010_012_valor_unitario desc limit 30;

#Granularidade por ano
insert into fatoVendasAtendGasto
select year(v.tb010_012_data), null, null, null, null, null, sum(v.tb010_012_quantidade) qtde, l.tb004_cod_loja, v.tb010_cpf, null,
sum(v.tb010_012_valor_unitario) valor, (v.tb010_012_valor_unitario/12) as media_por_mes
from tb010_012_vendas v, tb012_produtos p, tb005_funcionarios f, tb004_lojas l 
where p.tb012_cod_produto = v.tb012_cod_produto AND v.tb005_matricula = f.tb005_matricula AND f.tb004_cod_loja = l.tb004_cod_loja
group by v.tb010_cpf, year(v.tb010_012_data) order by v.tb010_012_valor_unitario desc limit 30;

# Fato Atendimentos
#Granularidade máxima
insert into fatoVendasAtendGasto
select year(tb010_012_data) ano, month(tb010_012_data) mes, day(tb010_012_data) dia, weekday(tb010_012_data),
null, null, count(tb010_012_data) qtde, null, null, tb005_matricula matricula, null, null
from tb010_012_vendas group by tb005_matricula, year(tb010_012_data), month(tb010_012_data), day(tb010_012_data);

# por mês
insert into fatoVendasAtendGasto
select year(tb010_012_data) ano, month(tb010_012_data) mes, null, null,
null, null, count(tb010_012_data) qtde, null, null, tb005_matricula matricula, null, null
from tb010_012_vendas group by tb005_matricula, year(tb010_012_data), month(tb010_012_data);

#Vendas por ano
insert into fatoVendasAtendGasto
select year(tb010_012_data) ano, null, null, null,
null, null, count(tb010_012_data) qtde, null, null, tb005_matricula matricula, null, null
from tb010_012_vendas group by tb005_matricula, year(tb010_012_data);


#Fato Lucratividade Bruta

# Granularidade maxima por ano e mes - Lucro por produto no de cada mes de cada ano

#Lucro por produto por mes de cada ano
insert into fatolucratividade
select AnoNum, mesAno, cod_prod ,(receita - custo) from tempFatoLucro;

#Lucro por produto por ano
insert into fatolucratividade
select AnoNum, null, cod_prod , (sum(receita)-sum(custo)) 'Lucro Anual' 
from tempFatoLucro group by cod_prod, AnoNum;


#Selects das tabelas
#select * from fatoVendasAtendGasto where cod_prod is not null and cod_categoria is not null;
#select * from fatoVendasAtendGasto where cpf is not null and valor is not null and media_gasto is not null and qtde is not null;
#select * from fatoVendasAtendGasto where matricula is not null and qtde is not null;
#select * from fatocompras;
#select * from fatoultimacompra;